The file contains code to create the figures used in main text and supplementary information of the paper 
**Climate policy accelerates structural changes in energy employment**.

To run the RMD file and see the resulting figures, press Knit on R studio (requires the package knitr), or else see the attached HTML file, already created through such a process.