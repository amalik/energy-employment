---
title: "Early just transition opportunities for coal-bearing states in India"
author: "Aman Malik"
date: "28-10-2021"
output:
  html_document: 
    toc: yes
    toc_float: yes
    keep_md: yes
editor_options: 
  chunk_output_type: console
---



#### Required packages



The following document provides high resolution figures, accompanying the main text and supplementary information. Note that these figures are unformatted and might differ slightly from the figures in the paper.

#### Loading external data







#### Coloring scheme



#### Data wrangling



# Figures

## Figure 1

![](Malik_et_al_2020_files/figure-html/figure1-1.png)<!-- -->

## Figure 2

![](Malik_et_al_2020_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

## Figure 3
### Preparing input data

#### India shape file state codes, state locations, and centroids



#### Technical potentials from NREL and NIWE



#### Commissioned and pipeline plants from Bridge to India



Joining above two and adding state locations to it



Adding additional ranges to eastern states potential



## Figure 3

![](Malik_et_al_2020_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

## Figure S1

### Figure S1



<img src="Malik_et_al_2020_files/figure-html/unnamed-chunk-10-1.png" width="100%" />

### Table S2


|State          |source |variable     | value|
|:--------------|:------|:------------|-----:|
|Rajasthan      |Solar  |Commissioned |   5.1|
|Rajasthan      |Wind   |Commissioned |   4.1|
|Gujarat        |Solar  |Commissioned |   2.6|
|Gujarat        |Wind   |Commissioned |   7.8|
|Andhra Pradesh |Solar  |Commissioned |   4.0|
|Andhra Pradesh |Wind   |Commissioned |   3.8|
|Karnataka      |Solar  |Commissioned |   7.6|
|Karnataka      |Wind   |Commissioned |   4.8|
|Tamil Nadu     |Solar  |Commissioned |   3.9|
|Tamil Nadu     |Wind   |Commissioned |   6.5|
|Maharashtra    |Solar  |Commissioned |   1.9|
|Maharashtra    |Wind   |Commissioned |   4.4|
|Madhya Pradesh |Solar  |Commissioned |   2.4|
|Madhya Pradesh |Wind   |Commissioned |   2.6|
|Odisha         |Solar  |Commissioned |   0.4|
|Chhatisgarh    |Solar  |Commissioned |   0.2|
|Jharkhand      |Solar  |Commissioned |   0.0|
|Odisha         |Wind   |Commissioned |   0.0|
|Chhatisgarh    |Wind   |Commissioned |   0.0|
|Jharkhand      |Wind   |Commissioned |   0.0|
|Telangana      |Solar  |Commissioned |   3.5|
|Telangana      |Wind   |Commissioned |   0.3|
|West Bengal    |Solar  |Commissioned |   0.1|
|West Bengal    |Wind   |Commissioned |   0.0|
|Rajasthan      |Solar  |Pipeline     |  27.5|
|Rajasthan      |Wind   |Pipeline     |   0.4|
|Gujarat        |Solar  |Pipeline     |   3.9|
|Gujarat        |Wind   |Pipeline     |   4.8|
|Andhra Pradesh |Solar  |Pipeline     |   7.5|
|Andhra Pradesh |Wind   |Pipeline     |   0.0|
|Karnataka      |Solar  |Pipeline     |   0.5|
|Karnataka      |Wind   |Pipeline     |   1.2|
|Tamil Nadu     |Solar  |Pipeline     |   1.0|
|Tamil Nadu     |Wind   |Pipeline     |   0.4|
|Maharashtra    |Solar  |Pipeline     |   2.9|
|Maharashtra    |Wind   |Pipeline     |   1.0|
|Madhya Pradesh |Solar  |Pipeline     |   0.3|
|Madhya Pradesh |Wind   |Pipeline     |   0.6|
|Odisha         |Solar  |Pipeline     |   0.0|
|Chhatisgarh    |Solar  |Pipeline     |   0.0|
|Jharkhand      |Solar  |Pipeline     |   0.1|
|Odisha         |Wind   |Pipeline     |   0.0|
|Chhatisgarh    |Wind   |Pipeline     |   0.0|
|Jharkhand      |Wind   |Pipeline     |   0.0|
|Telangana      |Solar  |Pipeline     |   0.2|
|Telangana      |Wind   |Pipeline     |   0.0|
|West Bengal    |Solar  |Pipeline     |   0.0|
|West Bengal    |Wind   |Pipeline     |   0.0|
|Rajasthan      |Solar  |Total        |  32.6|
|Rajasthan      |Wind   |Total        |   4.5|
|Gujarat        |Solar  |Total        |   6.5|
|Gujarat        |Wind   |Total        |  12.6|
|Andhra Pradesh |Solar  |Total        |  11.5|
|Andhra Pradesh |Wind   |Total        |   3.8|
|Karnataka      |Solar  |Total        |   8.1|
|Karnataka      |Wind   |Total        |   6.0|
|Tamil Nadu     |Solar  |Total        |   4.9|
|Tamil Nadu     |Wind   |Total        |   6.9|
|Maharashtra    |Solar  |Total        |   4.8|
|Maharashtra    |Wind   |Total        |   5.4|
|Madhya Pradesh |Solar  |Total        |   2.7|
|Madhya Pradesh |Wind   |Total        |   3.2|
|Odisha         |Solar  |Total        |   0.4|
|Chhatisgarh    |Solar  |Total        |   0.2|
|Jharkhand      |Solar  |Total        |   0.1|
|Odisha         |Wind   |Total        |   0.0|
|Chhatisgarh    |Wind   |Total        |   0.0|
|Jharkhand      |Wind   |Total        |   0.0|
|Telangana      |Solar  |Total        |   3.7|
|Telangana      |Wind   |Total        |   0.3|
|West Bengal    |Solar  |Total        |   0.1|
|West Bengal    |Wind   |Total        |   0.0|

### Table S4


|State          |Code |location        | solar_potential_gw| wind_potential_gw| avg_cap_factor|
|:--------------|:----|:---------------|------------------:|-----------------:|--------------:|
|Rajasthan      |RJ   |Western states  |              15179|               128|          0.182|
|Gujarat        |GJ   |Western states  |               3469|               143|          0.181|
|Madhya Pradesh |MP   |NA              |                109|                15|          0.176|
|Maharashtra    |MH   |Southern states |               3114|                98|          0.180|
|Karnataka      |KA   |Southern states |               1486|               124|          0.180|
|Tamil Nadu     |TN   |Southern states |                148|                69|          0.180|
|Andhra Pradesh |AP   |Southern states |                718|                75|          0.180|
|Telangana      |TG   |Southern states |                111|                25|          0.178|
|Odisha         |OR   |Eastern states  |                182|                 8|          0.166|
|Jharkhand      |JH   |Eastern states  |                 23|                 0|          0.168|
|Chhattisgarh   |CT   |Eastern states  |                 14|                 0|          0.173|
|West Bengal    |WB   |Eastern states  |                 85|                 1|          0.162|

### Figure S3

![](Malik_et_al_2020_files/figure-html/unnamed-chunk-13-1.png)<!-- -->
